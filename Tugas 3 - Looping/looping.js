// Soal 1
console.log("LOOPING PERTAMA");
var flag = 2;
while(flag <= 20){
    if(flag%2==0){
        console.log(flag + ' - I love coding');
    }
    flag++;
}
console.log("LOOPING KEDUA");
var flag = 20;
while(flag >= 2){
    if(flag%2==0){
        console.log(flag + ' - I will become a mobile developer');
    }
    flag--;
}

// Soal 2
for(var flag = 1; flag<=20; flag++){
    if(flag%3==0&&flag%2!=0){
        console.log(flag+' - I Love Coding');
    }
    else if(flag%2==0){
        console.log(flag+' - Berkualitas');
    }
    else if(flag%2!=0){
        console.log(flag+' - Santai');
    }
}

// Soal 3
for(var flag=4;flag>0;flag--){
    console.log('#'.repeat(8));
}

// Soal 4
var flag = 1;
while(flag<=7){
    console.log('#'.repeat(flag));
    flag++;
}

// Soal 5
var flag = 8;
while(flag>0){
    if(flag%2==0){
        console.log(' #'.repeat(4));
    }
    if(flag%2!=0){
        console.log('# '.repeat(4));
    }
    flag--;
}